/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.swing01;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class HelloMe {
    public static void main(String[] args) {
        
        JFrame frame = new JFrame("Hello ME");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        
        JLabel lblYourName = new JLabel("Your Name : ");
        lblYourName.setSize(80,20);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.white);
        lblYourName.setOpaque(true);
        frame.add(lblYourName);
        
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(90, 5);
        frame.add(txtYourName);
        
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(90,40);
        frame.add(btnHello);
        
        JLabel lblHello = new JLabel("Hello... ",JLabel.CENTER);
        lblHello.setSize(200,20);
        lblHello.setLocation(90, 70);
        lblHello.setBackground(Color.white);
        lblHello.setOpaque(true);
        frame.add(lblHello);
        
        btnHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                lblHello.setText("Hello "+name);
            }
        });
        
        frame.setLayout(null);
        frame.setVisible(true);
    }
    
}
